<?php get_header() ?>
	<div class="container"> 
 		<h1 class="errorpage"><i class="fa fa-exclamation-triangle"></i> Ups! Error <span>404</span></h1>
 		<h3 class="errorpageh3">It looks like that page no longer exists. Would  you like to go to <a href="<?php bloginfo('url');?>">homepage?</a></h3>
	</div>
<?php get_footer();?>