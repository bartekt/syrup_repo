

<?php get_header() ?>


<?php
//$category ="places";
// $category ="events"; // EVENTS 
if ($category =="places") {
	
	?>
	<!-- PLACES -->
	<div class="container" >
		 <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 pull-left">
		        <?php custom_breadcrumbs(); ?>
		    </div>   
		 </div> 
		 <div class="col-md-12  sub_header" style="background:url(<?php bloginfo('template_url');?>/images/temp.jpg)no-repeat; background-size:cover; background-position: center center;">	
		 	<div class="row"> 
			 	<div class="col-md-5 dark_label">
				 <h1>
				 	ELEN GALLERY
				 </h1>
				 <span>PRICE: <strong>20$</strong></span>
				 <span>LOCATION: <strong>ATHENS</strong></span> 
				 <div class="tag">
				 	<ul>
				 		<li>#tag</li>
				 		<li>#tag</li>
				 		<li>#tag</li>
				 	</ul>
				 </div>

				 <div class="cat_box">
				 	<ul>
				 		<li class="green">C1</li>
				 		<li class="yellow">C2</li>
				 		<li class="blue">C3</li>
				 		<li class="pink">C4</li>
				 	</ul>
				 </div>
			 	</div>
		 	</div> 
		 </div>
		</div>

		<div class="clear"></div>

		<div class="container  content_prw"> 
			<div class="row" id="icons-toolbar"> 
		    		<div class="col-md-6 col-sm-6 col-xs-12">  
						<ul class="author pull-left">
							<li><i class="fa fa-calendar"></i> <?php echo get_the_date('d  M  Y'); ?></li> 
						</ul>
		    		</div>
			    	<div class="col-md-6 col-sm-6 col-xs-12">
			    		<button class="print" onClick="printdiv('print');" >Print <span class="fa fa-print"></span></button>
		          <ul class="share">
		            <li>Share:</li>
		            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?> " title="Share on Facebook" target="_blank" class="fa fa-facebook"></a></li>
		            <li><a href="http://twitter.com/home?status=<?php the_permalink(); ?> " title="Share on Twitter" target="_blank" class="fa fa-twitter"></a></li>
		            <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=<?php the_permalink(); ?> " title="Share on LinkedIn" target="_blank" class="fa fa-linkedin"></a></li>
		            <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?> " title="Share on Google+" target="_blank" class="fa fa-google-plus"></a></li>
		            <li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php the_permalink(); ?> "
		              title="Share by Email" class="fa fa-envelope-o"></a></li>
		          </ul>
			    	</div>
		    	</div> 
			<div class="row">
				<div class="col-md-3 sidebar ">

					<div class="label_arrow_box"> 
						<h3>Location</h3> 
					</div> 
					<div class="info_box">
						<span><strong>LONDON, UK</strong></span>
						<span>21 street Green 22019</span> 
					</div>
					
					<div class="label_arrow_box">
						<h3>Location map</h3> 
					</div>
					<div class="map_iframe">
							<iframe
							  width="100%"
							  height="100"
							  frameborder="0" style="border:0"
							  src="https://www.google.com/maps/embed/v1/place?key=YOUR_API_KEY
							    &q=Space+Needle,Seattle+WA" allowfullscreen>
							</iframe>
						</div> 
					<div class="label_arrow_box">
						<h3>information</h3>  
					</div>
					<div class="info_box">
						<span>FEED:<strong>FREE</strong></span>
						<span>CHILD:<strong>YES</strong></span>
						<span>ANIMAL:<strong>NO</strong></span>
						<span>SPECIAL BOOKING:<strong>NO</strong></span>
					</div>
					
					<div class="label_arrow_box">
						<h3>download</h3> 
					</div>
					<div class="info_box">
						<ul class="download_links">
							<li><i></i>Document1</li>
							<li><i></i>Document1</li>
						</ul>
					</div>

				</div>
					<div class="col-md-3 sidebar gallery_side">
					<div class="label_arrow_box">
						<h3>Gallery</h3> 
					</div> 
				 
					 <img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375">
					  <img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375">
					   <img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375">
					    <img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375">			 

				</div>
					<div class="col-md-3 sidebar"> 
					<div class="label_arrow_box">
						<h3>Event's Calendar</h3> 
					</div> 
						<ul class="eve_cal">
							<li>
								<p>Aprol 24,2016</p>
								<span>Event colori title</span>
							</li>
							<li>
								<p>Aprol 24,2016</p>
								<span>Event colori title</span>
							</li>
							<li>
								<p>Aprol 24,2016</p>
								<span>Event colori title</span>
							</li>
							<li>
								<p>Aprol 24,2016</p>
								<span>Event colori title</span>
							</li>
							<li>
								<p>Aprol 24,2016</p>
								<span>Event colori title</span>
							</li>
						</ul> 	 
					</div>

				<div class="col-md-3  content_single" id="print"> 
					<p>Vivamus in dolor ultricies, <a href="http://dev.intelart.pl/darwin-digital-website/single-news-events.php#">tristique sapien quis</a>, mattis nisi. Vestibulum et facilisis est. Aliquam in maximus ipsum, ac elementum nisl. Nunc tincidunt <i>dui nibh, eu malesuada orci condimentum et</i>. Aenean link sodales ante lorem, a luctus metus commodo blandit. Etiam elementum risus et cursus tincidunt. Integer ac pretium felis. Pellentesque facilisis nunc at tortor ullamcorper posuere. Proin quis aliquam dui, in vestibulum enim. Fusce placerat, magna sed tempus mattis, ipsum tellus congue ante, venenatis mollis orci nulla et enim. Nulla facilisi. Proin placerat posuere vulputate.</p>
		        </div> 
			</div>  
		</div>
	<?php

}else{
	
	?>
	<!-- EVENT -->
		<div class="container" >
		 <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 pull-left">
		        <?php custom_breadcrumbs(); ?>
		    </div>   
		 </div> 
		 <div class="col-md-12  sub_header" style="background:url(<?php bloginfo('template_url');?>/images/temp.jpg)no-repeat; background-size:cover; background-position: center center;">	
		 	<div class="row"> 
			 	<div class="col-md-5 dark_label event">
				 <h1>
				 	ELEN GALLERY
				 </h1>
				  
				 <div class="tag event">
				 	<ul>
				 		<li>Child:NO</li>
				 		<li>Child:NO</li>
				 		<li>Child:NO</li>
				 	</ul>
				 </div>

				 <div class="cat_box">
				 	<ul>
				 		<li class="green">C1</li>
				 		<li class="yellow">C2</li>
				 		<li class="blue">C3</li>
				 		<li class="pink">C4</li>
				 	</ul>
				 </div>
			 	</div>
		 	</div> 
		 </div>
		</div>

		<div class="clear"></div>

		<div class="container  content_prw"> 
			<div class="row" id="icons-toolbar"> 
		    		<div class="col-md-6 col-sm-6 col-xs-12">  
						<ul class="author pull-left">
							<li><i class="fa fa-calendar"></i> <?php echo get_the_date('d  M  Y'); ?></li> 
						</ul>
		    		</div>
			    	<div class="col-md-6 col-sm-6 col-xs-12">
			    		<button class="print" onClick="printdiv('print');" >Print <span class="fa fa-print"></span></button>
		          <ul class="share">
		            <li>Share:</li>
		            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?> " title="Share on Facebook" target="_blank" class="fa fa-facebook"></a></li>
		            <li><a href="http://twitter.com/home?status=<?php the_permalink(); ?> " title="Share on Twitter" target="_blank" class="fa fa-twitter"></a></li>
		            <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=<?php the_permalink(); ?> " title="Share on LinkedIn" target="_blank" class="fa fa-linkedin"></a></li>
		            <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?> " title="Share on Google+" target="_blank" class="fa fa-google-plus"></a></li>
		            <li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php the_permalink(); ?> "
		              title="Share by Email" class="fa fa-envelope-o"></a></li>
		          </ul>
			    	</div>
		    	</div> 
			<div class="row">
				<div class="col-md-3 sidebar ">

					<div class="label_arrow_box"> 
						<h3>Location</h3> 
					</div> 
					<div class="info_box">
						<span><strong>LONDON, UK</strong></span>
						<span>21 street Green 22019</span> 
					</div>
					
					<div class="label_arrow_box">
						<h3>Location map</h3> 
					</div>
					<div class="map_iframe">
							<iframe
							  width="100%"
							  height="100"
							  frameborder="0" style="border:0"
							  src="https://www.google.com/maps/embed/v1/place?key=YOUR_API_KEY
							    &q=Space+Needle,Seattle+WA" allowfullscreen>
							</iframe>
						</div> 
					<div class="label_arrow_box">
						<h3>date</h3>  
					</div>
					<div class="info_box"> 
						<span>START:<strong>15 jun 2015</strong></span>
						<span>END:<strong>18 jun 2015</strong></span> 
					</div>
					
					<div class="label_arrow_box">
						<h3>download</h3> 
					</div>
					<div class="info_box">
						<ul class="download_links">
							<li><i></i>Document1</li>
							<li><i></i>Document1</li>
						</ul>
					</div>

				</div>
					<div class="col-md-3 sidebar gallery_side">
					<div class="label_arrow_box">
						<h3>Gallery</h3> 
					</div> 
				 
					 <img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375">
					  <img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375">
					   <img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375">
					    <img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375">			 

				</div> 

				<div class="col-md-6  content_single" id="print"> 
					<p>Vivamus in dolor ultricies, <a href="http://dev.intelart.pl/darwin-digital-website/single-news-events.php#">tristique sapien quis</a>, mattis nisi. Vestibulum et facilisis est. Aliquam in maximus ipsum, ac elementum nisl. Nunc tincidunt <i>dui nibh, eu malesuada orci condimentum et</i>. Aenean link sodales ante lorem, a luctus metus commodo blandit. Etiam elementum risus et cursus tincidunt. Integer ac pretium felis. Pellentesque facilisis nunc at tortor ullamcorper posuere. Proin quis aliquam dui, in vestibulum enim. Fusce placerat, magna sed tempus mattis, ipsum tellus congue ante, venenatis mollis orci nulla et enim. Nulla facilisi. Proin placerat posuere vulputate.</p>
		        </div> 
			</div>  
		</div>
	<?php

}

?> 
<?php get_footer();?>