	<div class="map"><!-- 1140x684--> 
	<img id="world-map" alt="World Map" src="<?php echo get_template_directory_uri(); ?>/images/dotted-world-map.jpg" />
    <?php $wp_query = new WP_Query; ?>
	<?php $articles=$wp_query->query(array('numberposts' => -1, 'posts_per_page'=> -1,'post_type' => 'travel', 'orderby'=>'menu_order','order'=>'ASC' )); ?>

	<?php if($articles){ ?>
	<?php foreach($articles as $a){ ?>
	<?php $post=get_post($a->ID); setup_postdata($post); ?>
		<?php
			$positions=explode(';',get_field('location'));
			if($positions){
				foreach($positions as $pos){
					$pos=explode(',',$pos);
					if($pos[0]){
						if(get_field('type')=='office') $status='office';
						else $status=get_field('status');
					if($a->post_name=='international-peacebuilding-advisory-team-ipat')
						$status='ipat';
		?>
			<a href="<?php the_permalink(); ?>" data-toggle="tooltip" data-placement="top" title="<?php the_title(); ?>" class="dot <?=$status?>" style="left:<?=$pos[0]?>%;top:<?=$pos[1]?>%;" data-sr></a>
		<?php 		}//end if($pos[0])
				}//end foreach $positions
			}//end if $positions
		?>
	<?php } /* end foreach $articles */ } /* end if $articles */ ?>
<!-- 	<div class="legend">
		<a href="<?=get_bloginfo('url')?><?=(get_current_blog_id()==7?'/donde-trabajamos/':'/where-we-work/')?>?type=office"><div class="dot office"></div> <div class="label office"><?php _e('Interpeace Office','geneva')?></div></a>
		<?php if(get_current_blog_id()==1){?>
		<a href="<?=get_bloginfo('url')?>/programme/international-peacebuilding-advisory-team-ipat/"><div class="dot ipat"></div> <div class="label ipat"><?php _e('IPAT Programme','geneva')?></div></a>
		<?php } ?>
		<a href="<?=get_bloginfo('url')?><?=(get_current_blog_id()==7?'/donde-trabajamos/':'/where-we-work/')?>?status=active"><div class="dot active"></div> <div class="label active"><?php _e('Active Programme','geneva')?></div></a>
		<a href="<?=get_bloginfo('url')?><?=(get_current_blog_id()==7?'/donde-trabajamos/':'/where-we-work/')?>?status=past"><div class="dot past"></div> <div class="label past"><?php _e('Past Programme','geneva')?></div></a>
	</div> -->
</div>