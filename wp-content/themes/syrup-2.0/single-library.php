<?php get_header() ?>

<div class="container" >
 <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 pull-left">
        <?php custom_breadcrumbs(); ?>
    </div>   
 </div> 
 <div class="col-md-12  sub_header" style="background:url(<?php bloginfo('template_url');?>/images/temp.jpg)no-repeat; background-size:cover; background-position: center center;">	
 	<div class="col-md-6">
 		<div class="sub_desc">
	 		<div class="stars"></div>
	 		<h2 class="heading">Heading post title</h2>
	 		<div class="category_box"></div>	
	 		<div class="avatar_big">
	 			<img src="<?php bloginfo('template_url');?>/images/avt.jpg"  >
	 		</div>
	 	</div>
 	</div> 
 </div>
</div>

<div class="clear"></div>

<div class="container  content_prw">
	<div class="row">
		<div class="col-md-3 sidebar">
			<div class="sidebar_first">
				<h3> John Smith </h3>
				<span> Dublin </span>
			</div>
			<div class="sidebar_tree">
				<i class="computer"></i> 
				<span> www.johny.com<br />  
				 johny@johny.com </span>
			</div>
			<div class="sidebar_tree">
				<i class="marker"></i>    
					<span> Dublin <br />
					21st Whilling Street<br />
					33-88AF Boston </span>
			</div>

			<div class="sidebar_tree">
				<i class="social_share"></i>  
					<span> 
						<a href="" class="fb"></a>
						<a href="" class="tw"></a>
						<a href="" class="ln"></a>
						<a href="" class="yt"></a>
					</span>
			</div>

		</div>

		<div class="col-md-9  content_single" id="print">
          
<div class="row" id="icons-toolbar"> 
    		<div class="col-md-6 col-sm-6 col-xs-12">  
				<ul class="author pull-left">
					<li><i class="fa fa-calendar"></i> <?php echo get_the_date('d  M  Y'); ?></li> 
				</ul>
    		</div>
	    	<div class="col-md-6 col-sm-6 col-xs-12">
	    		<button class="print" onClick="printdiv('print');" >Print <span class="fa fa-print"></span></button>
          <ul class="share">
            <li>Share:</li>
            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?> " title="Share on Facebook" target="_blank" class="fa fa-facebook"></a></li>
            <li><a href="http://twitter.com/home?status=<?php the_permalink(); ?> " title="Share on Twitter" target="_blank" class="fa fa-twitter"></a></li>
            <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=<?php the_permalink(); ?> " title="Share on LinkedIn" target="_blank" class="fa fa-linkedin"></a></li>
            <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?> " title="Share on Google+" target="_blank" class="fa fa-google-plus"></a></li>
            <li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php the_permalink(); ?> "
              title="Share by Email" class="fa fa-envelope-o"></a></li>
          </ul>
	    	</div>
    	</div>
			<p>Vivamus in dolor ultricies, <a href="http://dev.intelart.pl/darwin-digital-website/single-news-events.php#">tristique sapien quis</a>, mattis nisi. Vestibulum et facilisis est. Aliquam in maximus ipsum, ac elementum nisl. Nunc tincidunt <i>dui nibh, eu malesuada orci condimentum et</i>. Aenean link sodales ante lorem, a luctus metus commodo blandit. Etiam elementum risus et cursus tincidunt. Integer ac pretium felis. Pellentesque facilisis nunc at tortor ullamcorper posuere. Proin quis aliquam dui, in vestibulum enim. Fusce placerat, magna sed tempus mattis, ipsum tellus congue ante, venenatis mollis orci nulla et enim. Nulla facilisi. Proin placerat posuere vulputate.</p>
            <p><a href="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png"><img class="alignnone size-full wp-image-96" src="http://darwin.dev/wp-content/uploads/2015/11/asset_city.png" alt="asset_city" width="619" height="375"></a></p>
			<h1>Iphone 7 serious? in H1</h1>
			<h4>Suspendisse volutpat pellentesque erat<strong>In hac habitasse</strong>. In hac habitasse platea dictumst. Nulla pretium sed massa eget tempus. Integer varius faucibus ligula nec ultricies. In in tristique<strong>In hac habitasse</strong> enim, non faucibus mi. Pellentesque habitant morbi tristique.</h4>
			<p>Suspendisse volutpat pellentesque erat. <strong>In hac habitasse</strong> platea dictumst. Nulla pretium sed massa eget tempus. Integer varius faucibus ligula nec ultricies. In in tristique enim, non faucibus mi.<strong>In hac habitasse</strong> Pellentesque this is strong text senectus et netus et malesuada fames ac turpis egestas. Pellentesque placerat malesuada sapien sit amet iaculis. Phasellus enim nibh, viverra placerat ipsum et, molestie condimentum justo. Proin lacinia interdum lacus vel bibendum. Nam a sollicitudin neque. Praesent eu placerat purus, nec vestibulum ex. Proin in consequat nulla. Ut em ultrices diam ac venenatis bibendum. Morbi ullamcorper tincidunt erat et accumsan.</p>
			<p>Vivamus in dolor ultricies, <a href="http://dev.intelart.pl/darwin-digital-website/single-news-events.php#">tristique sapien quis</a>, mattis nisi. Vestibulum et facilisis est. Aliquam in maximus ipsum, ac elementum nisl. Nunc tincidunt <i>dui nibh, eu malesuada orci condimentum et</i>. Aenean link sodales ante lorem, a luctus metus commodo blandit. Etiam elementum risus et cursus tincidunt. Integer ac pretium felis. Pellentesque facilisis nunc at tortor ullamcorper posuere. Proin quis aliquam dui, in vestibulum enim. Fusce placerat, magna sed tempus mattis, ipsum tellus congue ante, venenatis mollis orci nulla et enim. Nulla facilisi. Proin placerat posuere vulputate.</p>
			<p>&nbsp;</p>
			<blockquote><p>Suspendisse volutpat pellentesque erat. In hac habitasse platea dictumst. Nulla pretium sed massa eget tempus. Integer varius faucibus ligula nec ultricies. In in tristique enim, non faucibus mi. Suspendisse volutpat pellentesque erat. In hac habitasse platea dictumst. Nulla pretium sed massa eget tempus. Integer varius faucibus ligula nec ultricies. In in tristique enim, non faucibus mi.Suspendisse volutpat pellentesque erat. In hac habitasse platea dictumst. Nulla pretium sed massa eget tempus. Integer varius faucibus ligula nec ultricies. In in tristique enim, non faucibus mi.</p></blockquote>
			<h2>THIS IS &lt; H2 &gt;</h2>
			<ul>
			<li>Suspendisse volutpat</li>
			<li>Pellentesque erat</li>
			<li>Lorem ipsum</li>
			<li>Dolor sit amet</li>
			</ul>
			<div id="attachment_196" style="width: 861px" class="wp-caption alignnone"><a href="http://darwin.dev/wp-content/uploads/2015/12/apps.jpg"><img class="size-full wp-image-196" src="http://darwin.dev/wp-content/uploads/2015/12/apps.jpg" alt="Muenster, Germany - March 30, 2013: A close up of an Apple iPhone 5 screen showing the App Store and various social media apps, including Google plus, Facebook, Pinterest, Twitter, LinkedIn and foursquare." width="851" height="564"></a><p class="wp-caption-text">Muenster, Germany – March 30, 2013: A close up of an Apple iPhone 5 screen showing the App Store and various social media apps, including Google plus, Facebook, Pinterest, Twitter, LinkedIn and foursquare.</p></div>
			 
			<h3>THIS IS &lt; H3 &gt;</h3>
			 

  			</div>


		  
	</div> 

	<?php get_template_part('last-post'); ?> 
	
</div>

<?php get_footer();?>