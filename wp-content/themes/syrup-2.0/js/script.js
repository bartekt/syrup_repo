$(document).ready(function(){
	
	var b,c,d,g,h;
	
	function a(){
		b=$(this).width();
		if(b>900)c=3;
		else c=1;
		$("#articles li").width($("#articles").width()/c-96);
		$("#bookmarks li").width($("#bookmarks ul").width()/c-32);
		if(b>900)d=12;
		else if(b>768)d=6;
		else if(b>600)d=4;
		else if(b>480)d=2;
		$("#numbering li").css({width:$("#numbering").width()/d-32,height:$("#numbering").width()/d-32});
		$(".map").each(function(){
			$(this).height(($(this).width()/4)*3);
		});
	}
	
	a();
	
	$(window).resize(a);
	
	function f(){
		if($(this).scrollTop()>0)$("#scroll").fadeIn();
		else $("#scroll").fadeOut();
		if($(this).scrollTop()>$(this).height()-$(window).height()-1)$("#news li:visible").next().show();
	}
	
	f();
	
	$(this).scroll(f);
	
	$(".navigation a").click(function(n){
		n.preventDefault();
		if(!$(this).parent().hasClass("active")){
			g=$(this).parent().index(),h=$(this).parents("ul").children().length-1;
			$(this).parents("ul").prev().children().fadeOut().eq(g).fadeIn();
			$(this).parents("ul").children().removeClass("active").eq(g).addClass("active");
		}
	});
	
	$("#scroll a").click(function(n){
		n.preventDefault();
		$("html,body").animate({scrollTop:0});
	});
	
	
	
	
	
	
	
	
	$("#button a").click(function(n){n.preventDefault()});
	
});
$(window).load(function(){
	
	function a(){
		$(".slideshow").each(function(){
			$(this).height($(this).children().outerHeight());
		});
		$("#slide").height($("#slide li").outerHeight());
	}
	
	a();
	
	$(this).resize(a);
	
});

// PRINT DIV 
function printdiv(print)
  {
    var headstr = "<html><head><title>Syrup.is</title></head><body>";
    var footstr = "</body>";
    var newstr = document.getElementById(print).innerHTML;
    var oldstr = document.body.innerHTML;
    document.body.innerHTML = headstr+newstr+footstr;
    window.print();
    document.body.innerHTML = oldstr;
    return false;
  }

