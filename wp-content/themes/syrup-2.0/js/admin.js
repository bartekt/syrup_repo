(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';

		$('.post-type-travel .term-description-wrap p.description').html('Specify the label for the button and the link separated with by the | symbol (e.g. Nuestra Pagina|http://www.interpeace-lao.org).');

/*
		$('#acf-field_56b62e8fbccb4').live("click",function(){
			var url=$('#url-field').val();
			var res=url.replace("glossary/","glossary#");
			res = res.substring(0, res.length - 1);
			$('#url-field').val(res);
		});
*/
		if($('html').attr('lang')=='es-ES')
			$('#acf-field_56b62e8fbccb4').parent().append('<div id="map" oncontextmenu="return false;"><img src="/wp-content/themes/syrup-2.0/images/dotted-world-map-latin.jpg"/><div id="point_1"></div></div>');
		else
			$('#acf-field_56b62e8fbccb4').parent().append('<div id="map" oncontextmenu="return false;"><img src="/wp-content/themes/syrup-2.0/images/dotted-world-map.jpg"/><div id="point_1"></div></div>');

		$('#acf-field_56b62e8fbccb4').on('change keyup',function(){
			var data = $('#acf-field_56b62e8fbccb4').val();
			var positions = data.split(';'); var key; var index=0;
			for (key in positions){
				index++;
				var arr=positions[key].split(',');
				if(!$('#point_'+index).length) $('#map').append('<div id="point_'+index+'"></div>');
				$('#point_'+index).css('top',arr[1]+'%').css('left',arr[0]+'%').draggable();		
			}
		});
		
		$( "#map div" ).live( "dragstop", function( event, ui ) {
			var position=$(this).position();
			var per_relativeX = position.left/$("#map img").width();
			var per_relativeY = position.top/$("#map img").height();
			update_coords($(this).attr('id').split('_')[1],(per_relativeX*100).toFixed(2),(per_relativeY*100).toFixed(2));
		});
		
		$( "#map div" ).live( "mousedown", function( event ) {
			 if( event.button == 2 ) {
				update_coords($(this).attr('id').split('_')[1],0,0);
				event.preventDefault();
				event.stopPropagation();
				return false;
			 }
			event.preventDefault();
			event.stopPropagation();
			return false;
		});
		
		/* -- update_coords(array_position,left_x_per,top_y_per) -- */
		function update_coords(arr_pos,left,top){
			var data = $('#acf-field_56b62e8fbccb4').val();
			var positions = data.split(';'); var key; var index=0;
			for (key in positions){
				index++;
				if(arr_pos==index) {
					positions[key]=left+','+top;
					if(left==0 && top==0) { positions[key]=null; $('#point_'+index).remove(); }		
				}
			}
			positions = positions.filter(function(n){ return n != undefined }); 
			data=positions.join(';');
		  	$('#acf-field_56b62e8fbccb4').val(data);		  
		}
		
		$("#map img").click(function(e) {
		  var data = $('#acf-field_56b62e8fbccb4').val();
		  var offset = $(this).offset();
		  var per_relativeX = (e.pageX - offset.left)/$("#map img").width();
		  var per_relativeY = (e.pageY - offset.top)/$("#map img").height();
		  //$('#acf-field_56b62e8fbccb4').val((per_relativeX*100).toFixed(2)+','+(per_relativeY*100).toFixed(2)).change();
		  $('#acf-field_56b62e8fbccb4').val(data+';'+(per_relativeX*100).toFixed(2)+','+(per_relativeY*100).toFixed(2)).change();
		  //alert("X: " + relativeX + "  Y: " + relativeY);
		});

		$('#acf-field_56b62e8fbccb4').keyup();
	});
	
})(jQuery, this);
