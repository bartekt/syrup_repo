<?php get_header() ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 pull-left">
		        <?php custom_breadcrumbs(); ?>
		    </div>   
		</div>  
 		<div class="row"> 
 			<div class="col-md-12">
 				<button class="btn btn-border pull-right">EDITORS MAIN</button>
 				<select name="type" id="type" class="pull-right select-filter">
 					<option value="1">Option1</option>
 					<option value="1">Option2</option>
 					<option value="1">Option3</option>
 				</select>
 			</div>
 		</div>
 		<div class="row">
 			<ul class="editors_list">
	 			<?php while ( have_posts() ) : the_post(); ?>
	 				<li> 
		 				<div class="edit_tmp">
		 					<div class="col-md-9 item">
						  		<div class="item_box" style="background:url('<?php bloginfo('template_url');?>/images/temp.jpg') center;background-size:cover">
					  				<div class="col-md-10 desc_cont"> 
										<h2 class="heading">Heading post title</h2>
										<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiLorem ipsum dolor sit amet.</p>
									</div>
								</div>
								<div class="label">
								<i class="ex1"></i>
								<p class="rotate">CATEGORY</p>
								</div>
							</div> 
							<div class="date">12/04/2015</div>
						</div>
	 				</li>
	 			<?php endwhile; // end of the loop. ?>	 
 			</ul>
 		</div>
	</div>
<?php get_footer();?>