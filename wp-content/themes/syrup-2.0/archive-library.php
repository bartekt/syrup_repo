<?php get_header() ?>

	<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 pull-left">
	        <?php custom_breadcrumbs(); ?>
	    </div>   
	</div> 
	<div class="col-md-12 subtitle"> 
		<div id="filter">
			<h2 class="left">Select city</h2>
			<select name="" class="left select">
				<option value="">Mexico</option>
				<option value="">Cracov</option> 
			</select>
			<input type="submit" value="Submit" class="right">
		</div>  
	</div>
	<?php $count = 0; // FIRST ELEMENT ?>
	<div id="articles" class="row">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php 
			
		    if ($count == '0') {
		     //LARGE BOX 
	    	 ?>
	    	<div class="col-md-12">
				<div class="item_box_full" style="background:url('<?php bloginfo('template_url');?>/images/temp.jpg') center;background-size:cover">
					<div class=" col-md-4">
						<div class="pull-left authores">
							<div class="avatars">
								<img src="<?php the_field('author_avatar'); ?>">
							</div>
							<div class="authores_des">
								<h3><?php the_field('author_name'); ?></h3>
								<span><?php the_field('author_city'); ?></span>
							</div>
						</div>
					</div> 
					<div class="col-md-8">
						<div class="pull-right first_item_desc"> 
							 <h2><?php the_title(''); ?></h2>
							 <?php the_content(''); ?>
						</div>
					</div> 
				</div> 
			</div>  
	    	 <?
	    	 $count ++; //COUNT ++
		    }else{
		    	
		    	//SMALL BOX
		    	?>
				<div class="col-md-4">
			  		<div class="item_box" style="background:url('<?php bloginfo('template_url');?>/images/temp.jpg') center;background-size:cover">
			  		  <div class="col-md-12">
							<div class="pull-left authores">
								<div class="avatars">
									<img src="<?php the_field('author_avatar'); ?>">
								</div>
								<div class="authores_des">
									<h3><?php the_field('author_name'); ?></h3>
									<span><?php the_field('author_city'); ?></span>
								</div>
							</div>
						</div>
						<div class="col-md-12 desc_cont">
							<p class="excerpt">Category</p>
							<h2 class="heading"><?php the_title(''); ?></h2>
						</div>
					</div>
				</div>	
		    <?
		    }
		    
		?> 
		<?php endwhile; // end of the loop. ?>
 
	</div> <!-- ROW -->
 </div><!-- CONTAINER-->

<?php get_footer();?>