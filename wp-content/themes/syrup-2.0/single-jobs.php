<?php get_header() ?>
	
<div class="container" id="job">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 pull-left">
	        <?php custom_breadcrumbs(); ?>
	    </div>   
	</div> 
	<div class="row">
	    	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4 "> 
				<img src="http://syrup.is/wp-content/uploads/2015/10/c224e7_8538e8e81d06d9ed30dcd1f8cdb78aaa-1.jpg_srz_p_110_103_75_22_0.50_1.20_0.00_jpg_srz8.jpeg" alt="logo">
	    	</div>
	    	<div class="col-lg-8">
				 <div class="company_name">
				 	<div class="job_name">
				 <h3><span>Job Title: </span>Junior Architect</h3>
				</div>
				<h3>Company info:</h3>
					<p><span> CN SOLUTIONS GROUP INC </span></p>
					<p>
						<span>
							Roberto							Cruz						</span>
					</p> 
					<p>
						<span>4400 Market St</span>
						<span>94602</span>
						<span>Oakland</span> 
						<span>USA</span>
					</p> 
					<p><span>+1 510 964 0860</span><span>robert.cn@me.com</span> </p> 
				 	<p></p>
				 </div> 
	    	</div>
	    </div>
	    <div class="row">
	    	<div class="col-lg-8">
	    		<div class="job_desc">
				 <h3>JOB DESCRIPTION</h3>
					<ul>
						<li>Access existing drawings and information from servers to work on assignments.</li>
						<li>Verify accuracy of the project information and measurements through site visit or information gathered by others.</li>
						<li>Maintain a knowledge base of current design solutions and construction techniques in order to contribute creative solutions tailored to individual client projects.</li>
						<li>Perform and/or verify research and due diligence for products and furnishing for applicable codes respective to each project application and location</li>
					</ul>
				</div>
	    	</div> 
	    	<div class="col-lg-4">
				<div class="row job_info"> 
			    	<div class="col-lg-12 must"> 
			    		<div class="label wp">
					 		<h3>work place: </h3>
					 	</div>
						<div class="boxx"> <h3><span class="boxes">USA</span>  <span>Oakland</span></h3>
					 
					</div> 
			    	</div>
			    	<div class="col-lg-12 must"> 
			    		<div class="label">
					 		<h3>salary: </h3>
					 	</div>
						 <div class="boxx"><h3><span>$80 /h</span></h3> </div>
			    	</div>
		    	</div>
	    	</div>
	     </div>
	     <div class="column">
		     <div class="row"> 
		    	<div class="col-lg-4 must">
					 <div>
					 	<div class="label">
					 		<h3>Required skills</h3>
					 	</div>
					 	<ul>
					 		<li>Proficient in computer-aided design (CAD), AutoCAD 2D is a must. Proficient in commercially available 3D structural modeling software, RISA 3D, STAAD, ETAB, RAM, SAFE</li>
					 	 
					 	</ul>
					 </div>
		    	</div>
		    	<div class="col-lg-4 must">
		    		<div>
						 <div class="label">
						 	<h3>Optional skills</h3>
						 </div>
						 <ul>
					 		<li>AutoCAD 3D </li> 
					 	</ul>
					 </div>
		    	</div> 
		    	<div class="col-lg-4 must">
		    		<div>
						<div class="label">
						 	<h3>Languages</h3>
						</div>
						<ul>
					 		<li>English, Russian, Spanish </li> 
					 	</ul>
					</div>
		    	</div> 
		     </div>
		     <div class="row">
		    	<div class="col-lg-4 center">
		    		<div>
						<button class="apply col-lg-4 " data-toggle="modal" data-target="#myApply">Apply</button>
					</div>
		    	</div> 
		     </div>
		   </div>	    
	</div>	

<?php get_footer();?>