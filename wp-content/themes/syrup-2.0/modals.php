<!-- LOGIN -->
        <div class="modal fade login" tabindex="-1" role="dialog" aria-labelledby="login">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title" id="gridSystemModalLabel">Login</h3>
            </div>
            
            <form>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                      </div>
                      <a href="#" class="forgot-pwd">Forgot password</a>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="submit" class="btn btn-pink btn-main-section">Login <span class="fa fa-long-arrow-right"></span></button>
              </div>
            </form>
            
            </div>
          </div>
        </div>


        <!-- PRIVACY POLICY -->

        <div class="modal fade policy" tabindex="-1" role="dialog" aria-labelledby="login">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="gridSystemModalLabel">Privacy Policy</h3>
              </div> 
              <div class="modal_box_in">

                <h3>SYRUP.IS LTD</h3>
                
                <h4>www.syrup.is Web Site Privacy Policy</h4>
                <p>
                PLEASE REVIEW THE PRIVACY POLICY FOR SYRUP.IS LTD www.syrup.is WEB SITE (THE "SITE"). BY USING THIS SITE, YOU AGREE TO FOLLOW AND BE BOUND BY THIS PRIVACY POLICY. IF YOU DO NOT AGREE WITH ANY OF THE TERMS OF THE PRIVACY POLICY, PLEASE DO NOT USE THIS SITE.
                </p>
                
                <h4>Privacy Policy</h4> 
                <p>This document describes how we may collect information from you or about you, why we collect this information, and how we may use or disclose this information. In addition, this document sets forth our general policies on information security.</p>
                
                <h4>Information Collection</h4>
                <p>There are several kinds of information we collect. For all visitors to the Site, we collect and log your IP address. An IP address is a number automatically assigned to your computer whenever you access the Internet. We collect IP address information to better administer our system and to gather aggregate information on visitors to our site, on how our site is being used, and on the pages visitors are viewing. This aggregate information may be shared with other organizations. To maintain your anonymity, we do not associate IP addresses with records containing personal information. We may use IP address information, however, to personally identify you in order to enforce our legal rights or when required to do so by law enforcement authorities. <br>
                 When you use Submit Form or Contact Form, you provide personal information, such as your name, email address, or phone number. The personal information of users who used Submit Form will be available on the Site.  
                </p>

                <h4>Cookies</h4>
                <p>We may also place small data files, called "cookies," in the browser file of your computer's hard drive. These cookies automatically identify your browser to our server when you interact with the Site. Most browsers automatically accept these cookies. We may use cookies to determine whether you are logged in to the Site, and to temporarily store application data</p>
              
                <h4>E-mail Communications, Newsletters and Related Sites</h4>
                <p>The Site provides you with the opportunity to receive communications from us or third parties. Once you have decided to receive such communications, you may later decide to stop receiving them. To stop receiving communications, please send email to up@syrup.is. Please be aware that e-mail communications may not be secure and may be susceptible to interception by third parties.</p>
                
                <h4>Children's Privacy</h4>
                <p>The Site is intended for adults aged 18 and older. It is not intended for or directed at persons under the age of 18. If you are under the age of 18, you may not submit personally identifiable information to or through the Site. SYRUP.IS LTD does not collect personally identifiable information through the Site from any person the SYRUP.IS LTD actually knows to be under the age of 18.</p>
                
                <h4>Other Sites</h4>
                <p>Our privacy policies apply only to your use of the Site. The Site contains links to third party sites, including sites that may indicate a special relationship with us. While we do not disclose personably identifiable information to the parties operating linked sites, we are not responsible for the privacy practices of such other sites. You should read the privacy policies of each site you visit to determine what information that site may be collecting about you.</p>
            
                <h4>Changes to Privacy Policy</h4>
                <p>Please review the Privacy Policy regularly. The Privacy Policy may change at any time. By using the Site after a change in the Privacy Policy, you agree to follow and be bound by the Privacy Policy as changed.</p>
              
              </div>
       
            
            </div>
          </div>
        </div>

<!-- COOKIE POLICY -->
        
        <div class="modal fade terms" tabindex="-1" role="dialog" aria-labelledby="terms">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="gridSystemModalLabel">Terms and condition</h3>
              </div> 
              <div class="modal_box_in">

                <p>PLEASE REVIEW THESE TERMS OF USE (THE "TERMS") FOR SYRUP.IS LTD www.syrup.is WEB SITE (THE "SITE"). BY USING THIS SITE, YOU AGREE TO FOLLOW AND BE BOUND BY THE TERMS. IF YOU DO NOT AGREE WITH ANY OF THE TERMS, PLEASE DO NOT USE THIS SITE.</p>
              <h4>Changes to Terms</h4>
              <p>Please review the Terms regularly. The Terms may change at any time. By using the Site after a change in the Terms, you agree to follow and be bound by the Terms as changed.</p><br>

              <h4>Table of Contents</h4>
                <ul>
                  <li>Site Content</li>
                  <li>Third Party Content</li>
                  <li>Your Use of the Site</li>
                  <li>Use by CN ROVN ENTERPRISES and SYRUP.IS LTD’ Employees</li>
                  <li>Use by Children</li>
                  <li>Links to Other Sites</li>
                  <li>User Comments and Submissions</li>
                  <li>Copyright</li>
                  <li>Privacy Policy</li>
                  <li>State Fundraising Notice</li>
                  <li>Disclaimers</li>
                  <li>Indemnification</li>
                  <li>Termination</li>
                  <li>Court</li>
                  <li>Miscellaneous</li> 
                </ul>
              <h4>Site Content</h4>
              <p>Unless otherwise expressly noted, you should assume everything you see, hear, or otherwise receive from or on the Site (the "Content") is copyright, trademark, trade dress or other intellectual property owned or licensed by. The Content includes, without limitation, images, illustrations, designs, icons, photographs, trademarks, logos, text, sounds, music, the Site as a whole and any other materials at the Site organized in following way: </p>


              <p>
              EDITORS – articles about art, design, architecture;
              <br>
              CORNERS - galleries profiles with information related to location, open hours, fees, description about the gallery and exhibitions that they have and their calendar;
              <br>
              EVENTS - events from galleries by date;
              <br>
              PLACES – galleries of cities;
              <br>
              LIBRARY -  all projects that we receive from all designers, architects and artists, including project description, screenshots, and references, organized in 9 different categories (politics, business, lifestyle, history, society, technology, ecology, health and entertainment);
              <br>
              CONTACT - a form to contact;
              <br>
              SUBMISSION FORM - where designers can submit their projects;
              <br>
              SEARCH
              <br>
              PHILOSHOPY  - SYRUP.IS LTD vision and concept about the project;
              <br><br>
              The information in this Site is provided as a courtesy by SYRUP.IS LTD. The views and opinions of individual authors do not necessarily state or reflect those of SYRUP.IS LTD. References to specific products, processes or services do not constitute or imply recommendation or endorsement by SYRUP.IS LTD.
              </p>
              <h4>Third Party Content</h4>
              <p>
              Nothing in the Terms transfers to you any right, title or interest in any Content, including any intellectual property or content of third parties included on the Site.
              </p>
              <h4>Your Use of the Site</h4>
              <p>
              You may use or download Content for research or educational purposes, or for your personal, noncommercial purposes, provided you keep unchanged all copyright and other notices with them. No other use of any Content is permitted unless a specific use is expressly sated as to particular Content.
              <br>You agree that you will make only lawful use of the Site, and will use the Site only in compliance with all federal, state and local laws and regulations. You agree that you will make no use of the Site that violates anyone else's rights, including copyright, trademark, patient, trade secret, privacy, publicity or other rights.
              <br>You agree that you will not upload, post, transmit, distribute or otherwise publish on or to the Site ("Publish") any materials that contain a software virus or other harmful component. You agree that you will not Publish any materials that contain advertising or commercial material of any kind. You agree that you will not Publish any materials that are false, threatening, libelous, defamatory, pornographic, obscene or otherwise unlawful, or that violate any rights of privacy or publicity or any trademark, copyright, patent or other rights. You are solely responsible for the content of any material you Publish. SYRUP.IS LTD is not responsible or liable for the conduct of any person using the Site.
              <br>SYRUP.IS LTD may change, suspend, discontinue or remove any aspect of the Site at any time, without prior notice, including the availability of any Site feature, services or Content or any material you have Published. SYRUP.IS LTD may restrict your access to certain features, services or Content at any time, without prior notice.
              </p>
              <h4> Use by CN ROVN ENTERPRISES and SYRUP.IS LTD’ Employees</h4>
              <p>
              If you are an employee, agent, contractor, or are otherwise employed by CN ROVN ENTERPRISES or SYRUP.IS LTD, these Terms supplement CN ROVN ENTERPRISES and SYRUP.IS LTD policy on Acceptable Use of CN ROVN ENTERPRISES and SYRUP.IS LTD Information Technology Resources. In the event of a conflict between these documents, the policy on Acceptable Use of CN ROVN ENTERPRISES and SYRUP.IS LTD Information Technology Resources will control.
              <br>Portions of the Site are not public. Use of non-public portions of the Site is restricted to authorized CN ROVN ENTERPRISES and SYRUP.IS LTD personnel and authorized scientific affiliates. All use of restricted portions of the Site may be monitored and/or recorded by CN ROVN ENTERPRISES and SYRUP.IS LTD system administration staff.
              <br>By using restricted portions of the Site, you expressly consent to any monitoring and recording of your activities while in such restricted areas. In the event monitoring reveals any indications of criminal activity, system administration staff will have the right, but not the obligation, to provide any such information to law enforcement personnel.
              </p>
              <h4>Use by Children</h4>
              <p>
              The Site is intended for adults aged 18 and older. It is not intended for or directed at persons under the age of 18. If you are under the age of 18, you may not register or submit personally identifiable information to or through the Site. SYRUP.IS LTD does not collect personally identifiable information through the Site from any person SYRUP.IS LTD actually knows to be under the age of 18.
              </p>

              <h4>Links to Other Sites</h4>
              <p>
              The Site may contain links to third party sites. Any outside links are provided only as a convenience. Your use of outside links is at your sole risk. Any links from the Site do not constitute SYRUP.IS LTD endorsement of any third party, its Web site, or its goods or services.
              <br>SYRUP.IS LTD is not responsible for any Web sites, services or other materials linked to or from the Site, and disclaims all liability for any injury you may experience by using such materials. If you have any concerns regarding any Web site linked to or from the Site, please direct them to the owner or operator of the Web site.
              <br>You may not create hyperlinks to the Site that portray SYRUP.IS LTD in a false or misleading light. You may not use any "framing" or similar techniques to enclose any portion of the Site.

              </p><h4>User Comments and Submissions</h4>
              <p>
              We welcome your comments regarding our Site and services. To submit your comments regarding our Site or services, e-mail them to up@syrup.is. However, if you are dissatisfied with the Site, any of its Content, or any of the Terms, your sole and exclusive legal remedy is to stop using the Site. Any comments, ideas, suggestions or other information you transmit to the Site are not confidential or proprietary. SYRUP.IS LTD will be free to use any comment, idea, suggestion or other information you transmit to us through the Site on an unrestricted basis. This may include personal information about you and your use of the Site. Please review SYRUP.IS LTD Privacy Policy for more information.
              </p>
              <h4>Copyright</h4>
              <p>
              SYRUP.IS LTD respects the intellectual property of others. If you believe that your work has been infringed on the Site, please send a statutory notice to SYRUP.IS LTD copyright agent for notice of claims of copyright infringement:
              <br>
              [Add individual's name if desired]
              <br>

              SYRUP.IS LTD<br>
              157 St John St<br>
              London <br>
              United Kingdom<br>
              EC1V 4PW<br>

              </p>


              <h4>Privacy Policy</h4>
              <p>
              SYRUP.IS LTD has a Privacy Policy that describes how we collect information from you or about you, why we collect this information, how we will use or disclose this information. In addition, SYRUP.IS LTD Privacy Policy sets forth our general policies on information security. 
              </p>
              <h4>Disclaimers</h4>
              <p>
              YOUR USE OF THE SITE IS AT YOUR OWN RISK. TO THE FULLEST EXTENT PERMISSIBLE BY LAW, SYRUP.IS LTD MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND IN CONNECTION WITH THE SITE. SYRUP.IS LTD PROVIDES THIS SITE, ALL CONTENTS, ALL INFORMATION AND SERVICES OFFERED ON OR THROUGH THE SITE "AS IS." SYRUP.IS LTD EXPRESSLY DISCLAIMS ALL EXPRESS OR IMPLIED WARRANTIES, INCLUDING BUT NOT LIMITED TO THE ACCURACY, COMPLETENESS, IMPLIED WARRANTIES OF MERCHANTABILITY, QUIET ENJOYMENT, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT OF ANY CONTENT OFFERED ON OR THROUGH THE SITE.
              <br>SYRUP.IS LTD WILL NOT BE LIABLE FOR ANY DAMAGES RELATED TO YOUR USE OF OR INABILITY TO USE THIS SITE, INCLUDING WITHOUT LIMITATION, DIRECT, INDIRECT, SPECIAL, COMPENSATORY OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOSS OF OR DAMAGE TO PROPERTY, EVEN IF FHCRC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
              <br>Some jurisdictions may not allow the exclusion of implied warranties, so some of the above exclusions may not apply to you. SYRUP.IS LTD does not warrant that your use of the Site, the operation or function of the Site, or any services offered on or through the Site, will be uninterrupted or error free, that defects will be corrected, or that the Site or its server are free of viruses or other harmful elements. SYRUP.IS LTD does not make any representations regarding the currency, accuracy or reliability of information on the Site.
              </p>
              <h4>Indemnification</h4>
              <p>
              You assume full responsibility for any use of any information, goods or services offered on or through the Site. You understand and agree that SYRUP.IS LTD will not be responsible or liable for any claim, loss or damage arising from the use of any information, goods or services.
              <br>You agree to defend, indemnify and hold harmless SYRUP.IS LTD and its officers, directors, owners, agents, employees, affiliates, licensees and licensors from and against any and all claims, damages, costs and expenses, including reasonable attorneys' fees, arising from or related to your use of the Site in violation of any of the Terms.
              </p>
              <h4>Termination</h4>
              <p>
              Your failure to comply with the Terms automatically revokes your authorization to use the Site and terminates any and all rights granted to you under the Terms. Your obligations to SYRUP.IS LTD will continue upon termination of your rights under the Terms, including restrictions regarding the Content, disclaimers, indemnification obligations and liability limitations under the Terms. Upon termination of your rights, you must promptly destroy all Content downloaded or obtained from this Site, as well as all copies of any Content.
              </p>
              <h4>Court</h4>
              <p>
              Any dispute arising under or relating to the Terms, the Content, the use of the Site, or any services obtained using this Site, will be resolved exclusively by the court relevant for SYRUP.IS LTD. Your use of the Site constitutes your consent to the jurisdiction and venue of such court with respect to any such dispute.
              </p>

              <h4>Miscellaneous</h4>
              <p>
              If for any reason a court of competent jurisdiction finds any provision of the Terms, or any portion thereof, to be unenforceable, that provision will be enforced to the maximum extent permissible so as to effect the intent of the Terms, and the remainder of the Terms will remain in full force and effect. The Terms are the entire agreement between you and SYRUP.IS LTD relating to the Site. Any other agreements between you and SYRUP.IS LTD regarding the subject matter of the Terms are superseded and have no force or effect. All rights not expressly granted herein are reserved.
              </p>
              
              </div>
       
            
            </div>
          </div>
        </div>