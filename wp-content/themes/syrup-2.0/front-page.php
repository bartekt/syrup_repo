<?php get_header();?>

	<div class="container">
		
		<!-- HOMEPAGE DESC -->
		<div class="row">
			<div class="col-md-12">
				<h1 class="center"><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit,</span><br /> sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. </h1> 
			</div>
			<div class="col-md-12">
				 <h2 class="title"><span class="fa fa-clock-o"></span> Last in syrup</h2>
			</div>		
		</div>
		<!-- :END  HOMEPAGE DESC -->

		<!-- LIBRARY -->
		<?php 
			$args = array(
	        'post_type' => 'library',
	        'librarys' => $term->slug,
	        'order' => 'ASC'
   		 );
		?>
		<div class="row">
			<div class="col-md-6"> 
					<div class="subtitle col-md-12">
						<h2 class="left"><span class="fa fa-th-large"></span>Exsplore Library</h2>
					</div> 
					<div class="col-md-12">
						<ul class="list slideshow"> 
						<?php $query = new WP_Query( $args ); while ( $query->have_posts() ) : $query->the_post(); ?> 
							<li class="item_hp">
								<a href="<?php the_permalink();?>">
									<div class="item_box row" style="background:url('<?php bloginfo('template_url');?>/images/temp.jpg') center;background-size:cover">
										<div class="col-md-3">
											<div class="pull-left authores">
												<div class="avatars">
													<img src="<?php the_field('author_avatar'); ?>">
												</div> 
											</div>
										</div>
										<div class="col-md-9 desc_cont">
											<p class="excerpt"><?php $term->name ?></p>
											<h2 class="heading"><?php echo get_the_title(); ?></h2>
											<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</a>
							</li>
						<?php  endwhile;?> 
						<?php wp_reset_postdata();   ?> 

						
						</ul>
						<ul class="list center navigation">
						<!-- LIBRARY NAV COUTNT FOR ACTIVE-->
							<li class="block active"><a href="" class="radius"></a></li>
							<li class="block"><a href="" class="radius"></a></li>
						<!-- :END -->	 
						</ul>   
				</div> 
			</div>
			<!-- :END LIBRAY --> 


			<div class="col-md-6"> 
				<div class="subtitle col-md-12">
					<h2 class="left"><span class="fa fa-th-large"></span>Event on this week in:</h2>
				</div>
				<div class="col-md-12 event_box">
			 		<ul>
			 			<li><a href="">Event Title Lorem Ipsum<span class="pull-right"> April/24/2015</span></a></li>
			 			<li><a href="">Event Title Lorem Ipsum<span class="pull-right"> April/24/2015</span></a></li>
			 			<li><a href="">Event Title Lorem Ipsum<span class="pull-right"> April/24/2015</span></a></li>
			 			<li><a href="">Event Title Lorem Ipsum<span class="pull-right"> April/24/2015</span></a></li>
			 			<li><a href="">Event Title Lorem Ipsum<span class="pull-right"> April/24/2015</span></a></li>
			 			<li><a href="">Event Title Lorem Ipsum<span class="pull-right"> April/24/2015</span></a></li>
			 			<li><a href="">Event Title Lorem Ipsum<span class="pull-right"> April/24/2015</span></a></li>
			 		</ul>
				</div> 
			</div>  
		</div>  
	</div>
	
	<!-- JOBS -->
	<?php 
		$args = array(
        'post_type' => 'jobs', 
        'order' => 'ASC',
        'posts_per_page' => 3
		 );
	?> 
	<div class="container">  
		<div class="subtitle col-md-12">
			<h2 class="left"><span class="fa fa-th-large"></span>Last jobs offer:</h2>
		</div>
		<div class="row"> 
			<?php $query = new WP_Query( $args ); while ( $query->have_posts() ) : $query->the_post(); ?> 
				<div class="col-md-4">
					<div  class="job_box">
						<span><?php echo get_the_date('d.m.Y'); ?></span>
						<p><?php echo get_the_title(); ?></p>
						<a class="best-btn" href="<?php the_permalink();?>">See More</a>
					</div> 
				</div>
			<?php  endwhile;?>  
			<?php wp_reset_postdata();   ?>   
		</div>  
	</div>
	<!-- :END JOBS --> 

	<div id="best" class"container-fluid">
		<div class="container center best"> 
		<div class="row">
			<div class="col-md-12">
				<div class="avatars">
					<img src="<?php bloginfo('template_url');?>/images/left.jpg">
				</div> 
			</div>
			<div class="col-md-12">
				<h3 class="block"><span class="fa fa-star-o"></span> Best on syrup</h3>
			</div>
			<div class="col-md-12">
				<div class="slide">
					<h2>Heading Post title</h2>
					<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.  </p>
				</div>
			</div>
			<div class="col-md-12">
				<a href="" class="block best-btn">see more <span class="fa fa-long-arrow-right"></span></a>
			</div>
		</div> 
		</div>
	</div> 

	<!--- editors and travel -->

	<div class="container"> 
		<div class="row">
		<!-- EDITORS -->
			<?php 
				$args = array(
		        'post_type' => 'editors', 
		        'order' => 'ASC'
				 );
			?>
			<div class="col-md-6"> 
					<div class="subtitle col-md-12">
						<h2 class="left"><span class="fa fa-th-large"></span>Read editors</h2>
					</div> 
					<div class="col-md-12">
						<ul class="list slideshow">

							<?php $query = new WP_Query( $args ); while ( $query->have_posts() ) : $query->the_post(); ?> 
								<li class="item_hp">
									<div class="item_box_v2 row" style="background:url('<?php bloginfo('template_url');?>/images/temp.jpg') center;background-size:cover">
										<div class="col-md-12">
											<div class="pull-left authores">
												<div class="avatars">
													<img src="<?php bloginfo('template_url');?>/images/left.jpg">
												</div> 
												<div class="authores_des">
													<h3>John Lenon</h3>
													<span>Dublin</span>
												</div>
											</div>
										</div>
										<div class="col-md-12 desc_cont"> 
											<h2 class="heading">Heading post title</h2>
											<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</li>
							<?php  endwhile;?> 
							

						</ul>

						<ul class="list center navigation">
							<li class="block active"><a href="" class="radius"></a></li>
							<li class="block"><a href="" class="radius"></a></li> 
						</ul>   
				</div> 
			</div>
			<?php wp_reset_postdata();   ?> 



			<div class="col-md-6"> 
					<div class="subtitle col-md-12">
						<h2 class="left"><span class="fa fa-th-large"></span>Telescope</h2>
					</div> 
					<div class="col-md-12">
						<ul class="list slideshow">
							<li class="item_hp">
								<div class="item_box_v2 row" style="background:url('<?php bloginfo('template_url');?>/images/temp.jpg') center;background-size:cover">
									<div class="col-md-12">
										<div class="pull-left authores">
											<div class="avatars">
												<img src="<?php bloginfo('template_url');?>/images/left.jpg">
											</div> 
											<div class="authores_des">
												<h3>John Lenon</h3>
												<span>Dublin</span>
											</div>
										</div>
									</div>
									<div class="col-md-12 desc_cont"> 
										<h2 class="heading">Heading post title</h2>
										<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									</div>
								</div>
							</li>

							<li class="item_hp">
								<div class="item_box_v2 row" style="background:url('<?php bloginfo('template_url');?>/images/temp.jpg') center;background-size:cover">
									<div class="col-md-12">
										<div class="pull-left authores">
											<div class="avatars">
												<img src="<?php bloginfo('template_url');?>/images/left.jpg">
											</div> 
											<div class="authores_des">
												<h3>Adam Eventual</h3>
												<span>Dublin</span>
											</div>
										</div>
									</div>
									<div class="col-md-12 desc_cont"> 
										<h2 class="heading">Heading post title</h2>
										<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									</div>
								</div>
							</li>

						</ul>
						<ul class="list center navigation">
							<li class="block active"><a href="" class="radius"></a></li>
							<li class="block"><a href="" class="radius"></a></li> 
						</ul>   
				</div> 
			</div>  

		</div>  
	</div>
 
<?php get_footer();?>