
	<div class="container-fluid background footer">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<ul class="lmenu_footer">
						<li><a href="#" type="button" data-toggle="modal" data-target=".policy">Privacy Policy</a></li>
						<li><a href="#" type="button" data-toggle="modal" data-target=".terms">Terms and Condition</a></li>
						<li><a href="/contact">Contact</a></li>
					</ul>
				</div>
				<div class="col-md-2"><a href="<?php bloginfo('url');?>"><img src="<?php bloginfo('template_url');?>/images/bottom.png" alt="logo"></a></div> 
				<div class="col-md-5">
					<ul class="rmenu_footer">
						<li><a>Syrup.io</a></li> 
					</ul>
				</div>
			</div>  
		</div>	
	</div>

	<p id="scroll"><a href="<?php bloginfo('url');?>"></a></p> 

	<?php get_template_part('modals'); ?>
	<?php wp_footer();?>

	 <!--  ***** JAVA SCRIPT *****   --> 
 

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/lib/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/lib/bootstrap.min.js"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/lib/retina.min.js"></script> 
        	<script src="<?php bloginfo('template_url');?>/js/script.js"></script>

</body>
</html>