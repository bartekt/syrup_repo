<?php get_header() ?>

	<div class="container contact_us">
		<div class="row">
			<div class="col-md-12">
				<h1 class="center"><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit,</span><br /> sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. </h1> 
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2 class="title">Contact Us</h2>
				</div>
			</div>
			<div class="col-md-12 contact_us_page">
		 		 <div class="col-md-6 contact_desc">
		 		 	
		 		 	<h3>ADVERTISIGN</h3>
		 		 	<p>ads@syrup.is</p>
		 		 	<h3>CONTACT</h3>
		 		 	<p>up@syrup.is</p>
		 		 	<h3>PRESS</h3>
		 		 	<p>press@syrup.is</p>
		 		 	<h3>EDITORIAL TEAM + GENERAL ISSUES</h3>
		 		 	<p>boost@syrup.is</p>
		 		 	<h3>ADDRESS</h3>
		 		 	<p>
		 		 		SYRUP.IS LTD<BR />
		 		 		157 ST. JOHNT STREET<BR />
		 		 		LONDON UK
		 		 	</p>

		 		 </div>
		 		 <div class="col-md-6 form"> 
			 		 <form>
			 		 <div class="row">
				 		 <div class="col-md-6">
					 		 <div class="form-group"> 
							 	<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Full name">
							 </div>
				 		 </div>  
				 		 <div class="col-md-6">
					 		 <div class="form-group"> 
							 	<input type="text" class="form-control" id="exampleInputEmail1" placeholder="E-Mail">
							 </div>
				 		 </div> 
			 		 </div>
			 		 <div class="row">
				 		 <div class="col-md-12">
					 		 <div class="form-group"> 
							 	<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Subject">
							 </div>
				 		 </div> 
			 		 </div>
			 		 <div class="row">
				 		 <div class="col-md-12">
					 		 <div class="form-group"> 
							 	<textarea class="form-control textarea" placeholder="Message"> </textarea>
							 </div>
				 		 </div> 
			 		 </div>  
					  <button type="submit" class="btn btn-default pull-right">Send message</button>
					</form> 
		 		 </div>
			</div>	
		</div>
	</div>

<?php get_footer();?>