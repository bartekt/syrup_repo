<?php 


// CUSTOM POST TYPE //

add_action( 'init', 'create_post_type' );

function create_post_type() { 

// JOBS //

	register_post_type( 'jobs',
		array(
			'labels' => array(
			'name' => __( 'Jobs' ),
			'singular_name' => __( 'jobs' ),
		 	 ),
			'public' => true,
			'supports' => array('title'),
			'capability_type' => 'post',
			'hierarchical' => true,
			'query_var' => true,
			'supports' => array('title','thumbnail',),
			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-nametag', 
		)
	);

// LIBRARY //
      register_taxonomy( 'librarys', 'library', array(
        'hierarchical' => true,
        'label' => 'Category',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'librarys/categories', 'with_front' => FALSE), 
      )
      ); 

    register_post_type( 'library',
        array(
          'labels' => array(
          'name' => __( 'Library' ),
          'singular_name' => __( 'library' ),
          ),

          'public' => true, 
      'capability_type' => 'post',
      'hierarchical' => true,
      'rewrite' => array('slug' => 'librarys','with_front' => FALSE),
      // 'query_var' => true,
      'supports' => array( 'title', 'editor','thumbnail', ),
      'public' => true,
      'has_archive' => true,

          'menu_icon' => 'dashicons-grid-view',  
        )
    
    );
    flush_rewrite_rules();

   // EDITORS //

    register_post_type( 'editors',
        array(
          'labels' => array(
          'name' => __( 'Editors' ),
          'singular_name' => __( 'editors' ),
          ),
          'public' => true,
          'supports' => array('title'),
          'capability_type' => 'post',
          'hierarchical' => true,
          'query_var' => true,
          'supports' => array('title','editor','author','thumbnail',),
          'public' => true,
          'has_archive' => true,
          'menu_icon' => 'dashicons-book-alt',  
        )
    );

    // TRAVEL CORNERS //

    register_post_type( 'travel',
        array(
          'labels' => array(
          'name' => __( 'Travel' ),
          'singular_name' => __( 'travel' ),
          ),
          'public' => true,
          'supports' => array('title'),
          'capability_type' => 'post',
          'hierarchical' => true,
          'query_var' => true,
          'supports' => array('title','editor','author','thumbnail',),
          'public' => true,
          'has_archive' => true,
          'menu_icon' => 'dashicons-location-alt',  
        )
    );
    register_taxonomy( 'travel-category', 'travel', array(
	      'hierarchical' => true,
	      'label' => 'Category',
	      'query_var' => true, 
	    )
    );  
} // END FUNSTION

// Breadcrumbs
function custom_breadcrumbs() {

  /* === OPTIONS === */
     $text['home']     = _x( 'Home / ', 'Home / ', 'pietergoosen' ); // text for the 'Home' link
     $text['category'] = __( 'Archive by Category "%s"', 'pietergoosen' );  // text for a category page
     $text['search']   = __( 'Search Results for "%s" Query', 'pietergoosen' ); // text for a search results page
     $text['tag']      = __( 'Posts Tagged "%s"', 'pietergoosen' );  // text for a tag page
     $text['author']   = __( 'Author %s', 'pietergoosen' ); // text for an author page
     $text['404']      = __( 'Error 404', 'pietergoosen' );  // text for the 404 page

     $show_current   = 1; // 1 - show current post/page/category title in breadcrumbs, 0 - don't show
     $show_on_home   = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
     $show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
     $show_title     = 1; // 1 - show the title for the links, 0 - don't show
     $delimiter      = ' / '; // delimiter between crumbs
     $before         = '<span class="current">'; // tag before the current crumb
     $after          = '</span>'; // tag after the current crumb
     /* === END OF OPTIONS === */

     global $post;
        $here_text    = __('');
     $home_link    = home_url('/');
     $link_before  = '<span typeof="v:Breadcrumb">';
     $link_after   = '</span>';
     $link_attr    = ' rel="v:url" property="v:title"';
     $link         = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
        if (isset($post)){
            $parent_id    = $parent_id_2  = $post->post_parent;
        }
     $frontpage_id = get_option('page_on_front');

     if (is_home() || is_front_page()) {

            if ($show_on_home == 1) echo '<div class="breadcrumb"><a href="' . $home_link . '">' . $text['home'] . '</a> / </div>';

        } else {

         echo '<div class="breadcrumb">';
         if ($show_home_link == 1) {
             echo  $here_text . '<a href="' . $home_link . '" rel="v:url" property="v:title">' . $text['home'] . '</a>';
             if ($frontpage_id == 0 || (!empty($parent_id) && $parent_id != $frontpage_id)) echo $delimiter;
         }

         if ( is_category() ) {
             $this_cat = get_category(get_query_var('cat'), false);
             if ($this_cat->parent != 0) {
                 $cats = get_category_parents($this_cat->parent, TRUE, $delimiter);
                 if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                 $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                 $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                 if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                 echo $cats;
             }
                if ($show_current == 1) echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

          } elseif ( is_search() ) {
              echo $before . sprintf($text['search'], get_search_query()) . $after;

          } elseif ( is_day() ) {
             echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
             echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
             echo $before . get_the_time('d') . $after;

         } elseif ( is_month() ) {
             echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
             echo $before . get_the_time('F') . $after;

          } elseif ( is_year() ) {
              echo $before . get_the_time('Y') . $after;

          } elseif ( is_single() && !is_attachment() ) {
                if ( get_post_type() != 'post' ) {
                    $post_type = get_post_type_object(get_post_type());
                    $slug = $post_type->rewrite;
                    printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
                    if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
                } else {
                    $cat = get_the_category(); $cat = $cat[0];
                    $cats = get_category_parents($cat, TRUE, $delimiter);
                    if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                    $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                    $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                    if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                    echo $cats;
                    if ($show_current == 1) echo $before . get_the_title() . $after;
                }

         } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
             $post_type = get_post_type_object(get_post_type());
             echo $before . $post_type->labels->singular_name . $after;

         } elseif ( is_attachment() ) {
               $parent = get_post($parent_id);
                $cat = get_the_category($parent->ID); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter);
                $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
              printf($link, get_permalink($parent), $parent->post_title);
              if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;

            } elseif ( is_page() && !$parent_id ) {
                if ($show_current == 1) echo $before . get_the_title() . $after;

            } elseif ( is_page() && $parent_id ) {
                if ($parent_id != $frontpage_id) {
                    $breadcrumbs = array();
                    while ($parent_id) {
                        $page = get_page($parent_id);
                        if ($parent_id != $frontpage_id) {
                            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                        }
                        $parent_id = $page->post_parent;
                    }
                   $breadcrumbs = array_reverse($breadcrumbs);
                   for ($i = 0; $i < count($breadcrumbs); $i++) {
                       echo $breadcrumbs[$i];
                       if ($i != count($breadcrumbs)-1) echo $delimiter;
                   }
              }
             if ($show_current == 1) {
                 if ($show_home_link == 1 || ($parent_id_2 != 0 && $parent_id_2 != $frontpage_id)) echo $delimiter;
                 echo $before . get_the_title() . $after;
             }

          } elseif ( is_tag() ) {
             echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

          } elseif ( is_author() ) {
               global $author;
               $userdata = get_userdata($author);
              echo $before . sprintf($text['author'], $userdata->display_name) . $after;

           } elseif ( is_404() ) {
               echo $before . $text['404'] . $after;
           }

          if ( get_query_var('paged') ) {
              if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
             echo __('&nbsp;&raquo;&nbsp; Page', 'pietergoosen') . ' ' . get_query_var('paged');
             if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
         }

           echo '</div><!-- .breadcrumbs -->';

        }

}
// end:Breadcrumbs

function custom_map() {
  echo '<style type="text/css">#map{position:relative} #map img{width:100%;height:100%;cursor:crosshair;} #map>div{cursor:pointer;display:block;position:absolute;top:0;left:0;height:4px;width:4px;background:#FFF;border:2px solid #338FBA;border-radius:4px;box-shadow:0 0 0 7px rgba(51,143,186,.5);margin-left:-4px;margin-top:-5px;}</style>';
}
add_action('admin_head', 'custom_map');

 if( !function_exists( "wp_admin_js" ) ) {  
  function wp_admin_js($hook){
  if ( $hook=='edit.php' || $hook=='post.php' || $hook=='edit-tags.php' ){
      wp_register_script( 'customadmin',get_template_directory_uri().'/js/admin.js', array('jquery'),'1.0',true );
      wp_enqueue_script( 'customadmin' );

      wp_register_script( 'customjqueryui',get_template_directory_uri().'/js/jquery-ui.min.custom.js', array('jquery'),'1.11',true );
      wp_enqueue_script( 'customjqueryui' );

  }
  }
}
add_action( 'admin_enqueue_scripts', 'wp_admin_js' );
  
?>