#!/bin/bash

CODE="syrup"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function doload {
    echo "Loading database dump..."
    mysql -u$CODE -p$CODE $CODE < $DIR/content.sql
}

echo -e "Loading from \e[4m$DIR\e[0m. Enter your mysql \e[4mroot\e[0m pasword if prompted. "

mysql -u$CODE -p$CODE -e "exit" >/dev/null 2>/dev/null
RETVAL=$?

if [ $RETVAL = 0 ]
then
    doload
else
    echo "Creating user and database..."
    mysql -uroot -p < ${DIR}/create.sql
    doload
fi

echo "Done."
