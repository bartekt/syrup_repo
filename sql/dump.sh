#!/bin/bash

CODE="syrup"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "Dumping to \e[4m$DIR\e[0m. Enter your mysql \e[4mroot\e[0m pasword if prompted."

mysqldump -u$CODE -p$CODE $CODE --no-create-db --add-drop-table --disable-keys --opt | sed -e $'s/),(/),\\\n(/g' > $DIR/content.sql
